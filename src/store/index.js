import Vue from "vue";
import Vuex from "vuex";
import EventService from "../services/EventService.js";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    shoeIcon: [
      "mdi-plus",
      "mdi-square",
      "mdi-sparkles",
      "mdi-alpha-c-box-outline",
      "mdi-alpha-g-circle",
    ],
    shoes: [],
    mood: "",
    page: 10,
  },
  mutations: {
    SET_SHOES(state, payload) {
      state.shoes = payload;
    },
    SUBMIT_SHOE(state, shoe) {
      state.shoes = shoe;
    },
    DELETE_SHOE(state, shoe) {
      state.shoes = shoe;
    },
    ADD_MOOD(state, mood) {
      state.mood = mood.mood;
    },
    SET_PAGE(state, page) {
      state.page = page.page;
    },
  },
  actions: {
    getShoes({commit}, page) {
      return EventService.getShoes(page).then((response) => {
        commit("SET_SHOES", response.data);
        commit("SET_PAGE", page);
      });
    },
    submitShoe({state, commit}, payload) {
      let res = new Object();
      let listGroup = new Array();

      function getShoeColorNum(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
      }

      let shoeIconNum = getShoeColorNum(0, state.shoeIcon.length);
      for (let i = 2; i < 6; i++) {
        if (payload[i] !== "") {
          let lists = new Object();
          lists["name"] = payload[i];
          lists["icon"] = state.shoeIcon[shoeIconNum];
          listGroup.push(lists);
        }
      }
      // res["id"] = state.shoes.length == 0 ? 1 : state.shoes.length + 1;
      res["title"] = payload[0];
      res["color"] = payload[1];
      res["location"] = payload[7];
      res["list"] = listGroup;
      return EventService.submitShoe(res)
        .then(() => {
          commit("SUBMIT_SHOE", res);
          console.log("SUCCESS", res);
        })
        .catch((error) => {
          console.log("error", error);
        });
    },
    deleteShoe({commit}, shoe) {
      return EventService.deleteShoe(shoe.id)
        .then(() => {
          commit("DELETE_SHOE", shoe);
        })
        .catch((error) => {
          console.log("error", error);
        });
    },
    addMood({commit}, mood) {
      let res = new Object();
      if (mood > 0 && mood < 30) {
        res["mood"] = "나쁨";
      } else if (mood > 31 && mood < 50) {
        res["mood"] = "보통";
      } else if (mood > 51 && mood < 80) {
        res["mood"] = "좋음";
      } else {
        res["mood"] = "아주좋음";
      }
      // res["id"] = state.shoes.length == 0 ? 1 : state.shoes.length;
      return EventService.addMood(res).then(() => {
        commit("ADD_MOOD", res);
      });
    },
  },
  getters: {
    shoes: (state) => {
      return state.shoes;
    },
    mood: (state) => {
      return state.mood;
    },
    page: (state) => {
      return state.page;
    },
  },
  modules: {},
});
