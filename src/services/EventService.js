import axios from "axios";
import store from "@/store";

const apiClient = axios.create({
  baseURL: `http://localhost:3000`,
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default {
  getShoes(page) {
    console.log("page:", page);
    return apiClient.get("/shoes");
  },
  submitShoe(shoe) {
    return apiClient.post("/shoes", shoe);
  },
  deleteShoe(shoe) {
    return apiClient.delete("/shoes/" + shoe);
  },
  addMood(mood) {
    return apiClient.patch("/mood/1", mood);
  },
};
